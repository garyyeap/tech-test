const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const low = require('lowdb');
const argv = require('yargs').argv;
const users = require('./src/users');
const jobs = require('./src/jobs');
const profile = require('./src/profile');

app.use(cors());
app.use(bodyParser.json());

const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('./db.json');
const db = low(adapter);

db.defaults({ users: {} }).write();

app.use(async function (req, res, next) {
  const token = req.header('token');

  const response = await fetch(`https://graph.facebook.com/v8.0/me?fields=id%2Cname%2Cemail&access_token=${token}`);

  if (response.status === 200) {
    const json = await response.json();

    req.validId = json.id;
    req.email = json.email;
    req.name = json.name;
  }

  console.log(response.status, 'fb', response)
  next();
});

const writeMethods = ['PUT', 'PATCH'];

const accessGuard = function (req, res, next) {
  const id = req.params.id;
  const user = db.get(`users[${id}]`).value();
  const isOwner = req.validId === req.params.id;

  if (!user) {
    return res.status(404).end();
  }

  if (writeMethods.includes(req.method) && !isOwner) {
    return res.status(401).end();
  }

  if (req.method === 'GET' && !isOwner && !user.account.share) {
    return res.status(401).end();
  }

  req.isOwner = isOwner;
  req.user = user;

  next();
};

app.use('/users/:id/:resource', accessGuard);
app.use('/users/:id', accessGuard);

users(app, db);
profile(app, db);
jobs(app, db);

const port = argv.port || 80;
app.listen(port, () => {
  console.log(`Listening at http://localhost:5566`);
});
