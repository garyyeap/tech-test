import { useMemo } from 'react';
import { SIZE } from '../constants';

const len = SIZE.length;

export default function (size: Size , adjust: number = 0): Size {
  return useMemo(function () {
    const index = SIZE.indexOf(size) + adjust;

    if (index < 0) return SIZE[0];
    if (index >= len) return SIZE[len - 1];

    return SIZE[index];
  }, [size]);
}
