import { useMemo } from 'react';
import { SIZE } from '../constants';

const len = SIZE.length;
const SIZE_MAP = {
  xxsmall: '18px',
  xsmall: '24px',
  small: '36px',
  medium: '36px',
  large: '48px',
  xlarge: '72px',
  xxlarge: '96px'
};

export default function (size: Size , adjust: number = 0): string {
  return useMemo(function () {
    const index = SIZE.indexOf(size) + adjust;

    if (index < 0) return SIZE_MAP[SIZE[0]];
    if (index >= len) return SIZE_MAP[SIZE[len - 1]];

    return SIZE_MAP[SIZE[index]];
  }, [size]);
}
