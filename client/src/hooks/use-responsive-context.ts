import { useContext } from 'react';
import { ResponsiveContext } from 'grommet';

export default function (): Size {
  return useContext(ResponsiveContext) as Size;
}
