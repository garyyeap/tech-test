export const _SIZE = [
  'xxsmall',
  'xsmall',
  'small',
  'medium',
  'large',
  'xlarge',
  'xxlarge'
] as const;

export const SIZE = [..._SIZE];

 // @ts-ignore: GLOBAL_API_HOST comes from webpack.DefinePlugin
export const API_HOST = window.location.protocol + '//' + GLOBAL_API_HOST;
// @ts-ignore: GLOBAL_API_HOST comes from webpack.DefinePlugin
export const FB_APP_ID = GLOBAL_FB_APP_ID;
