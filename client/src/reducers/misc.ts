import produce, { Draft } from 'immer';
import {
  SET_CONTINUE_PATH,
  NOTIFY,
  RESTORE_ERROR,
  RESTORE_SUCCESS,
  RESTORE_PENDING,
  DISMISS
} from '../actions/misc';

const initState: MiscState = {
  continuePath: null
};

const reducer = produce(function (draft: Draft<MiscState>, action) {
  switch (action.type) {

    case SET_CONTINUE_PATH:
      draft.continuePath = action.continuePath;
      break;

    case NOTIFY:
      draft.message = action.message;
      break;

    case DISMISS:
      draft.message = null;
      break;

    case RESTORE_PENDING:
      draft.isRestorePending = true;
      break;

    case RESTORE_ERROR:
    case RESTORE_SUCCESS:
      draft.isRestorePending = false;
      break;
  }
}, initState);

export default reducer;
