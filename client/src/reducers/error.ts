import produce, { Draft } from 'immer';
import { HAS_ERROR, DISMISS_ERROR } from '../actions/error';

const initState: ErrorState = {
  hasError: false,
  reason: null,
  redirect: null
};

const reducer = produce(function (draft: Draft<ErrorState>, action) {
  switch (action.type) {
    case HAS_ERROR:
      draft.hasError = true;
      draft.reason = action.reason;
      draft.redirect = action.redirect;
      break;

    case DISMISS_ERROR:
      draft.hasError = false;
      draft.reason = null;
      draft.redirect = null;
      break;
  }
}, initState);

export default reducer;
