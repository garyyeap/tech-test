import { combineReducers } from 'redux';
import error from './error';
import auth from './auth';
import profile from './profile';
import jobs from './jobs';
import misc from './misc';
import account from './account';

const rootReducer = combineReducers({
  profile,
  jobs,
  account,
  auth,
  error,
  misc
});

export default rootReducer;
