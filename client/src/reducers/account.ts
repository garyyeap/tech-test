import produce, { Draft } from 'immer';
import {
  FETCH_ACCOUNT_SUCCESS,
  FETCH_ACCOUNT_ERROR,
  FETCH_ACCOUNT_PENDING,
  PATCH_ACCOUNT_SUCCESS,
  PATCH_ACCOUNT_ERROR,
  PATCH_ACCOUNT_PENDING
} from '../actions/account';

const initState: AccState = {
  data: {},
  isPending: false,
  hasError: false,
  reason: null
};

const reducer = produce(function (draft: Draft<AccState>, action) {
  switch (action.type) {
    case PATCH_ACCOUNT_PENDING:
    case FETCH_ACCOUNT_PENDING:
      draft.isPending = true;
      draft.hasError = false;
      break;

    case PATCH_ACCOUNT_SUCCESS:
    case FETCH_ACCOUNT_SUCCESS:
      draft.isPending = false;
      draft.hasError = false;
      draft.data = action.data;
      break;

    case PATCH_ACCOUNT_ERROR:
    case FETCH_ACCOUNT_ERROR:
      draft.isPending = false;
      draft.hasError = true;
      draft.data = {};
      break;
  }
}, initState);

export default reducer;
