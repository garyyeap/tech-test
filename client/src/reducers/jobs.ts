import produce, { Draft } from 'immer';
import {
  FETCH_JOBS_PENDING,
  FETCH_JOBS_ERROR,
  FETCH_JOBS_SUCCESS,
  UPDATE_JOBS_PENDING,
  UPDATE_JOBS_ERROR,
  UPDATE_JOBS_SUCCESS
} from '../actions/jobs';

const initState: JobsState = {
  data: [],
  isPending: false,
  hasError: false,
  reason: null
};

const reducer = produce(function (draft: Draft<JobsState>, action) {
  switch (action.type) {
    case FETCH_JOBS_PENDING:
    case UPDATE_JOBS_PENDING:
      draft.isPending = true;
      draft.hasError = false;
      draft.reason = null;
      break;

    case FETCH_JOBS_SUCCESS:
    case UPDATE_JOBS_SUCCESS:
      return {
        isPending: false,
        hasError: false,
        data: action.data
      };

    case FETCH_JOBS_ERROR:
    case UPDATE_JOBS_ERROR:
      draft.isPending = false;
      draft.hasError = true;
      draft.reason = action.reason;
      break;
  }
}, initState);

export default reducer;
