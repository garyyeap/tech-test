import produce, { Draft } from 'immer';
import {
  FB_LOGOUT_PENDING,
  FB_LOGOUT_SUCCESS,
  FB_LOGIN_PENDING,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_ERROR
} from '../actions/auth';

const removeSession = function () {
  window.localStorage.removeItem('vendor');
  window.localStorage.removeItem('token');
  window.localStorage.removeItem('id');
  window.localStorage.removeItem('tokenExpires');
};

const setSession = function (action: any) {
  window.localStorage.setItem('vendor', 'fb');
  window.localStorage.setItem('tokenExpires', action.tokenExpires);
  window.localStorage.setItem('token', action.token);
  window.localStorage.setItem('id', action.uid);
};

const tokenExpires = parseInt(window.localStorage.getItem('tokenExpires'), 10);
if (tokenExpires < Date.now()) {
  removeSession();
} else {
  setTimeout(removeSession, tokenExpires - Date.now() - 1000 * 60 * 5);
}

const initState: AuthState = {
  hasError: false,
  isPending: false,
  reason: null,
  vendor: window.localStorage.getItem('vendor'),
  token: window.localStorage.getItem('token'),
  uid: window.localStorage.getItem('id'),
  tokenExpires: window.localStorage.getItem('tokenExpires')
};

const reducer = produce(function (draft: Draft<AuthState>, action) {
  switch (action.type) {
    case FB_LOGIN_PENDING:
    case FB_LOGOUT_PENDING:
      return {
        hasError: false,
        isPending: true,
        reason: null,
        vendor: 'fb',
        token: null,
        uid: null,
        tokenExpires: null
      };

    case FB_LOGIN_SUCCESS:
      setSession(action);

      return {
        hasError: false,
        isPending: false,
        reason: null,
        vendor: 'fb',
        token: action.token,
        uid: action.uid,
        tokenExpires: action.tokenExpires
      };

    case FB_LOGIN_ERROR:
      removeSession();
      return {
        hasError: true,
        isPending: false,
        reason: action.reason,
        vendor: 'fb',
        token: null,
        uid: null,
        tokenExpires: null
      };

    case FB_LOGOUT_SUCCESS:
      removeSession();
      return {
        hasError: false,
        isPending: false,
        reason: null,
        vendor: null,
        token: null,
        uid: null,
        tokenExpires: null
      };
  }
}, initState);

export default reducer;
