import produce, { Draft } from 'immer';
import {
  FETCH_PROFILE_PENDING,
  FETCH_PROFILE_ERROR,
  FETCH_PROFILE_SUCCESS,
  UPDATE_PROFILE_PENDING,
  UPDATE_PROFILE_ERROR,
  UPDATE_PROFILE_SUCCESS
} from '../actions/profile';

const initState: ProfileState = {
  data: {},
  isPending: false,
  hasError: false,
  reason: null
};

const reducer = produce(function (draft: Draft<ProfileState>, action) {
  switch (action.type) {
    case FETCH_PROFILE_PENDING:
    case UPDATE_PROFILE_PENDING:
      draft.isPending = true;
      draft.hasError = false;
      draft.reason = null;
      break;

    case FETCH_PROFILE_SUCCESS:
    case UPDATE_PROFILE_SUCCESS:
      return {
        isPending: false,
        hasError: false,
        data: action.data
      };

    case FETCH_PROFILE_ERROR:
    case UPDATE_PROFILE_ERROR:
      draft.isPending = false;
      draft.hasError = true;
      draft.reason = action.reason;
      break;
  }
}, initState);

export default reducer;
