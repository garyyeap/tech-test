import React, { useMemo, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import { createSelector } from 'reselect';
import { useTranslation } from 'react-i18next';
import { hasError } from './actions/error';
import { setContinuePath } from './actions/misc';
import { fetchAccount } from './apis/account';
import LoadingCover from './components/loading-cover';

const selectAuth = createSelector(
  (state: State) => state.auth,
  (auth: AuthState) => auth
);

const selectAccount = createSelector(
  (state: State) => state.account,
  (account: AccState) => account
);

export default function<T> (WrappedComponent: React.ComponentType<T>) {
  return React.memo(function (props: T) {
    const dispatch = useDispatch();
    const { pathname: path } = useLocation();
    const { uid } = useSelector(selectAuth);
    const { data, isPending } = useSelector(selectAccount);
    const { t } = useTranslation('error');
    const [type, urlId] = useMemo(() => path.substring(1).split('/'), [path]);

    const element = useMemo(function () {
      if (isPending) {
        return <LoadingCover show={true}/>
      }
      
      if (type === 'edit' && uid !== urlId) {
        if (!uid) {
          dispatch(hasError(t('login'), '/login'));
          dispatch(setContinuePath(path));
        } else {
          dispatch(hasError(t('permission'), '/'));
        }
        return <div />;
      }

      if (type === 'view' && data.share === false && uid !== urlId) {
        if (!uid) {
          dispatch(hasError(t('login'), '/login'));
          dispatch(setContinuePath(path));
        } else {
          dispatch(hasError(t('permission'), '/'));
        }
        return <div />;
      }

      return <WrappedComponent { ...props} />;
    }, [path, isPending]);

    useEffect(function () {
      if (urlId) dispatch(fetchAccount(urlId));
    }, [urlId]);

    return element;
  });
}
