import React from 'react';
import { Box, Grid } from 'grommet';
import { Switch, Route } from 'react-router-dom';
import useResponsiveContext from './hooks/use-responsive-context';
import Header from './components/header';
import Login from './components/login';
import Profile from './components/profile';
import Jobs from './components/jobs';
import View from './components/view';
import Home from './components/home';
import Restore from './components/restore';
import withAuthGuard from './with-auth-guard';

const Layout = function () {
  const size = useResponsiveContext();

  return (
    <Grid
      rows={['auto', 'auto']}
      columns={['flex']}
      areas={[
        { name: 'header', start: [0, 0], end: [1, 0] },
        { name: 'main', start: [0, 1], end: [1, 1] }
      ]}
      responsive={true}
      justify="center"
    >
      <Box id="header" gridArea="header" fill="horizontal"><Header /></Box>
      <Box id="main" gridArea="main" direction="column" justify="start" width={size}>
        <Restore />
        <Switch>
          <Route path="/login"><Login /></Route>
          <Route path="/edit/:id">
            <Profile />
            <Jobs />
          </Route>
          <Route path="/view/:id">
            <View />
          </Route>
          <Route path="*">
            <Home />
          </Route>
        </Switch>
      </Box>
    </Grid>
  );
};

export default withAuthGuard(Layout);
