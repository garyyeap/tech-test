import { createSelector } from 'reselect';

export const selectProfile = createSelector(
  (state: State) => state.profile,
  (profile: ProfileState) => profile
);

export const selectJobs = createSelector(
  (state: State) => state.jobs,
  (jobs: JobsState) => jobs
);

export const selectAuth = createSelector(
  (state: State) => state.auth,
  (auth: AuthState) => auth
);

export const selectAccount = createSelector(
  (state: State) => state.account,
  (account: AccState) => account
);

export const selectMisc = createSelector(
  (state: State) => state.misc,
  (misc: MiscState) => misc
);
