import { grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';

const theme = deepMerge(grommet, {
  global: {
    breakpoints: {
      xxsmall: { value: 360 },
      xsmall: { value: 480 },
      small: { value: 640 },
      medium: { value: 740 },
      large: { value: 1280 },
      xlarge: { value: 1536 },
      xxlarge: { value: 1920 }
    },
    size: {
      xxsmall: '320px',
      xsmall: '360px',
      small: '480px',
      medium: '740px',
      large: '1280px',
      xlarge: '1400px',
      xxlarge: '1536px',
      full: '100'
    }
  },
  layer: {
    background: 'none'
  },
  icon: {
    size: {
      xxsmall: '12px',
      xsmall: '12px',
      small: '18px',
      medium: '24px',
      large: '32px',
      xlarge: '48px',
      xxlarge: '48px'
    }
  },
  avatar: {
    size: {
      xxsmall: '36px',
      xsmall: '48px',
      small: '64px',
      medium: '96px',
      large: '128px',
      xlarge: '156px',
      xxlarge: '172px'
    },
    extend: () => 'align-items: center;'
  },
  formField: {
    border: undefined,
    content: { pad: 'large' },
    label: {
      margin: {
        vertical: 'xsmall',
        horizontal: 'xsmall'
      }
    },
    error: {
      margin: {
        vertical: 'xsmall',
        horizontal: 'xsmall'
      },
      size: 'small'
    }
  },
  text: {
    xxsmall: {
      size: '12px',
      height: '18px',
      maxWidth: '288px'
    }
  }
});

export default theme;
