import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { Button, Main, Box } from 'grommet';

const selectAuth = createSelector(
  (state: State) => state.auth,
  (auth: AuthState) => auth
);

export default function () {
  const history = useHistory();
  const { uid } = useSelector(selectAuth);
  const { t } = useTranslation('misc');
  const create = useCallback(function () {
    const path = uid ? `/view/${uid}` : '/login';
    history.push(path);
  }, [history]);

  return (
    <Main pad="large">
      <Box direction="row" justify="center" pad="xlarge">
        <Button alignSelf="center" secondary={true} label={t('register')} onClick={create} />
      </Box>
    </Main>
  );
}
