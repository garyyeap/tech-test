import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect'
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Heading,
  Layer
} from 'grommet';
import LoadingCover from './loading-cover';
import { restoreBackup, cancelBackup } from '../apis/backup';

const selectAuth = createSelector(
  (state: State) => state.auth,
  (auth: AuthState) => auth
);

const selectMisc = createSelector(
  (state: State) => state.misc,
  (misc: MiscState) => misc
);

let hasBackup = !!window.localStorage.getItem('has-backup');

export default function () {
  const dispatch = useDispatch();
  const history = useHistory();
  const { uid } = useSelector(selectAuth);
  const { continuePath, isRestorePending } = useSelector(selectMisc);
  const { t } = useTranslation('misc');
  const [showRestore, setShowRestore] = useState(hasBackup && !!uid);
  const cancel = useCallback(function () {
    dispatch(cancelBackup());
    setShowRestore(false);
    hasBackup = false;
    uid && history.push(continuePath || `/view/${uid}`);
  }, []);

  const restore = useCallback(function () {
    dispatch(restoreBackup());
    setShowRestore(false);
    hasBackup = false;
  }, []);

  return (
    <React.Fragment>
      {showRestore &&
      <Layer onEsc={cancel} onClickOutside={cancel} responsive={false}>
        <LoadingCover show={isRestorePending}>
          <Card background="light-1">
            <CardHeader pad="medium">
              <Heading margin="none" level="3">{t('restore')}</Heading>
            </CardHeader>
            <CardBody pad="medium">{t('confirm-restore')}</CardBody>
            <CardFooter pad="small" justify="end" background="light-4">
              <Button label={t('cancel')} onClick={cancel} />
              <Button primary={true} label={t('ok')} onClick={restore} />
            </CardFooter>
          </Card>
        </LoadingCover>
      </Layer>}
    </React.Fragment>
  );
}
