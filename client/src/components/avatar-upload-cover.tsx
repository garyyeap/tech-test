import React from 'react';
import { Avatar, Stack } from 'grommet';
import { FormUpload } from 'grommet-icons';
import styled from 'styled-components';
import getAdjustIconSize from '../hooks/use-adjust-icon-size';

const AvatarCover = styled(Stack)`
  cursor: pointer;
  & .upload-icon-cover {
    opacity: 0.6;
  }
`;

interface AvatarUploadCoverProps {
  size: Size;
  children: JSX.Element[] | JSX.Element;
}
export default function ({ size, children }: AvatarUploadCoverProps) {
  const bgSize = getAdjustIconSize(size, -2);
  const cameraIconSize = getAdjustIconSize(size, -3);

  return (
    <AvatarCover className="avatar-cover" anchor="bottom-right" alignSelf="center">
      {children}
      <Avatar
        className="upload-icon-cover"
        background="dark-1"
        size={bgSize}
        alignSelf="center"
      >
        <FormUpload className="upload-icon" size={cameraIconSize} color="#FFFFFF" />
      </Avatar>
    </AvatarCover>
  );
}
