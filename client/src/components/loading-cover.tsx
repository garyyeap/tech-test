import React from 'react';
import styled from 'styled-components';
//  @ts-ignore
import Loading from 'react-loading-components';

const LoadingCover = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: rgba(255, 255, 255, .5);
  z-index: 999;
`;

interface LoadingCoverProps {
  show: boolean;
  children?: JSX.Element[] | JSX.Element;
}
export default function ({ show, children }: LoadingCoverProps) {

  return (
    <div style={{ position: 'relative', minHeight: '200px' }}>
      {children}
      {show && <LoadingCover>
        <Loading type="three_dots" width={70} height={70} fill="#444" />
      </LoadingCover>}
    </div>
  );
}
