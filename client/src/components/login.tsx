import React, { useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect'
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
// @ts-ignore: facebook sdk
import { fbLogin } from '../apis/facebook';
import LoadingCover from './loading-cover';
import { selectAuth, selectMisc } from '../reselect';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Heading,
  Layer
} from 'grommet';


export default function () {
  const dispatch = useDispatch();
  const history = useHistory();
  const { uid, isPending } = useSelector(selectAuth);
  const { continuePath } = useSelector(selectMisc);
  const { t } = useTranslation('auth');
  const cancel = useCallback(function () {
    history.push('/');
  }, []);

  const login = useCallback(function () {
    dispatch(fbLogin());
  }, []);

  useEffect(function () {
    uid && history.push(continuePath || `/view/${uid}`);
  }, [uid]);

  return (
    <Layer onEsc={cancel} onClickOutside={cancel} responsive={false}>
      <LoadingCover show={isPending}>
        <Card background="light-1">
          <CardHeader pad="medium">
            <Heading margin="none" level="3">{t('login')}</Heading>
          </CardHeader>
          <CardBody pad="medium">{t('login-to-facebook')}</CardBody>
          <CardFooter pad="small" justify="end" background="light-4">
            <Button label={t('cancel')} onClick={cancel} />
            <Button primary={true} label={t('ok')} onClick={login} />
          </CardFooter>
        </Card>
      </LoadingCover>
    </Layer>
  );
}
