import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from 'reselect';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Heading,
  Layer
} from 'grommet';
import { dismissError } from '../actions/error';

const selectError = createSelector(
  (state: State) => state.error,
  (error: ErrorState) => error
);

export default function () {
  const dispatch = useDispatch();
  const { t } = useTranslation('error');
  const history = useHistory();
  const { hasError, reason, redirect } = useSelector(selectError);
  const dismiss = useCallback(function () {
    dispatch(dismissError());

    if (redirect) history.push(redirect);
  }, [redirect]);

  return (
    <React.Fragment>
      {hasError && <Layer onEsc={dismiss} onClickOutside={dismiss} responsive={false}>
        <Card background="light-1">
          <CardHeader pad="medium">
            <Heading margin="none" level="3">{t('error')}</Heading>
          </CardHeader>
          <CardBody pad="medium">{reason}</CardBody>
          <CardFooter pad="small" justify="end" background="light-4">
            <Button primary={true} label={t('ok')} onClick={dismiss} />
          </CardFooter>
        </Card>
      </Layer>}
    </React.Fragment>
  );
}
