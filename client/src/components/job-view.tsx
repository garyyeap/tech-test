import React from 'react';
import styled from 'styled-components';
import { Organization } from 'grommet-icons';
import { useTranslation } from 'react-i18next';
import {
  Avatar,
  Box,
  Paragraph,
  FormField,
  Text
} from 'grommet';
import useAdjustSize from '../hooks/use-adjust-size';
import useAdjustIconSize from '../hooks/use-adjust-icon-size';
import useResponsiveContext from '../hooks/use-responsive-context';

const FormFieldText = styled(FormField)`
  flex-grow: 1;
  & label { margin: 5px 0; font-weight: bold };
`;

export default function ({ job }: { job: Job }) {
  const size = useResponsiveContext();
  const avatarSize = useAdjustSize(size, -1);
  const iconSize = useAdjustIconSize(avatarSize);
  const { t } = useTranslation('job');

  return (
    <Box className="job" direction="column" fill="horizontal">
      <Box className="comppany" direction="row">
        <Box
          className="company-logo"
          pad="small"
          alignSelf="center"
        >
          {job.logoUrl ?
            <Avatar src={job.logoUrl} size={avatarSize} />
          : <Avatar background="status-unknown" size={avatarSize}>
              <Organization size={iconSize}/>
            </Avatar>
          }
        </Box>
        <Box
          className="company-name"
          pad="small"
          flex="grow"
          direction="row"
          align="center"
        >
          <FormFieldText label={t('company-name')}>
            <Text>{job.company}</Text>
          </FormFieldText>
        </Box>
      </Box>
      <Box className="job-title" direction="row" pad="small">
        <FormFieldText label={t('job-title')}>
          <Text>{job.title}</Text>
        </FormFieldText>
      </Box>
      <Box
        className="date"
        alignContent={job.present ? "between" : "start"
      } 
        direction="row" pad="small" gap="small">
        <FormFieldText label={t('start-month')}>
          <Text>{job.startMonth}</Text>
        </FormFieldText>
        <FormFieldText label={t('start-year')}>
          <Text>{job.startYear}</Text>
        </FormFieldText>
        {job.present
          ? <Text>{t('present')}</Text>
          : <React.Fragment>
            <FormFieldText label={t('end-month')}>
              <Text>{job.endMonth}</Text>
            </FormFieldText>
            <FormFieldText label={t('end-year')}>
              <Text>{job.endYear}</Text>
            </FormFieldText>
          </React.Fragment>
        }
      </Box>
      <Box
        className="job-description"
        direction="row"
        pad="small"
        gap="small"
      >
        <FormFieldText label={t('job-description')}>
          <Paragraph>{job.description}</Paragraph>
        </FormFieldText>
      </Box>
    </Box>
  );
}
