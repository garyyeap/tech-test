import React, { useMemo, useEffect, useCallback, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import produce, { Draft } from 'immer';
import { useImmer } from 'use-immer';
import { createSelector } from 'reselect';
import { Button, Header, Box, Footer, Heading, Form } from 'grommet';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import LoadingCover from './loading-cover';
import { fetchJobs, updateJobs } from '../apis/jobs';
import Job from './job';

const EMPTY_JOB: Job = {
  _uid: null,
  title: '',
  startYear: '',
  endYear: '',
  startMonth: '',
  endMonth: '',
  present: false,
  description: '',
  logoUrl: '',
  company: ''
};

const addId = function (draft: Draft<JobsState>) {
  const uid = Date.now();

  if (draft.data.length === 0) {
    draft.data.push({ ...EMPTY_JOB, _uid: uid });
    return;
  }

  draft.data = draft.data.map(function (job, i) {
    job._uid = uid + i;
    return job;
  });
};

const selectJobs = createSelector(
  (state: State) => state.jobs,
  (jobs: JobsState) => produce(jobs, addId)
);

export default function () {
  const dispatch = useDispatch();
  const pendingUpdateJobs = useRef([]);
  const { id }: { id: string } = useParams();
  const { data, isPending } = useSelector(selectJobs);
  const [localData, setLocalData] = useImmer(data);
  const { t } = useTranslation('jobs');

  const cacheJob = useCallback(function (i, job) {
    pendingUpdateJobs.current[i] = job;
  }, []);

  const removeJob = useCallback(function (i) {
    pendingUpdateJobs.current.splice(i, 1);
    setLocalData(draft => { draft.splice(i, 1); });
  }, []);

  const submit = useCallback(function () {
    const body = produce(pendingUpdateJobs.current, function (draft) {
      draft.map(function (job) {
        delete job._uid;
        return job;
      });
    });

    dispatch(updateJobs(id, body));
  }, []);

  const addJob = useCallback(function () {
    const emptyJob = { ...EMPTY_JOB, _uid: Date.now() };

    pendingUpdateJobs.current.unshift(emptyJob);
    setLocalData(draft => { draft.unshift(emptyJob); });
  }, [setLocalData]);

  const { jobs, showSubmit } = useMemo(function () {
    return {
      showSubmit: localData.length > 0,
      jobs: localData.map(function (job, index) {
        return <Job
          job={job}
          key={job._uid}
          index={index}
          onChange={cacheJob}
          onRemove={removeJob}
        />;
      })
    };
  }, [localData, cacheJob, removeJob]);

  useEffect(function () {
    dispatch(fetchJobs(id));
  }, [id]);

  useEffect(function () {
    if (!isPending) {
      setLocalData(() => data);
    }
  }, [isPending, data]);

  return (
    <LoadingCover show={isPending}>
      <Header
        pad="medium"
        justify="between"
        direction="row"
      >
        <Heading truncate={true} level="2">{t('experiences')}</Heading>
        <Button secondary={true} label={t('add')} onClick={addJob} />
      </Header>
      <Form onSubmit={submit}>
        <Box width="100%" direction="column">{jobs}</Box>
        {showSubmit && <Footer pad="medium" justify="end" direction="row">
          <Button primary={true} label={t('submit')} type="submit" />
        </Footer>}
      </Form>
    </LoadingCover>
  );
}
