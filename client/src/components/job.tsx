import React, { useState, useCallback, useEffect } from 'react';
import { useImmer } from 'use-immer';
import styled from 'styled-components';
import { Organization } from 'grommet-icons';
import { useTranslation } from 'react-i18next';
import {
  Avatar,
  Box,
  Button,
  CheckBox,
  Select,
  TextArea,
  TextInput,
  FormField,
  Footer
} from 'grommet';
import useAdjustSize from '../hooks/use-adjust-size';
import useAdjustIconSize from '../hooks/use-adjust-icon-size';
import useResponsiveContext from '../hooks/use-responsive-context';
import AvatarUploadCover from './avatar-upload-cover';
import LogoEditor from './avatar-editor';

const YEARS = [new Date().getFullYear(), ...Array(69)].map((_val, i, arr) => {
  return arr[0] - 69 + i;
});

const FullWidthFormField = styled(FormField)`
  flex-grow: inherit;
  margin: 0;
  width: 100%;
`;

interface JobProps {
  job: Job;
  index: number;
  onChange: (i: number, job: Job) => void;
  onRemove: (i: number) => void;
}
export default function ({ job: initJob, onChange, onRemove, index }: JobProps) {
  const size = useResponsiveContext();
  const avatarSize = useAdjustSize(size, -1);
  const iconSize = useAdjustIconSize(avatarSize);
  const { t } = useTranslation('job');
  const [job, setJob] = useImmer(initJob);
  const [isPresent, setIsPresent] = useState(job.present);
  const [showLogoEditor, setShowLogoEditor] = useState(false);

  const toogleLogoEditor = useCallback(function () {
    setShowLogoEditor(!showLogoEditor);
  }, [showLogoEditor]);

  const toggleIsPresent = useCallback(function () {
    setIsPresent(!isPresent);
    setJob(draft => { draft.present = !isPresent });
  }, [isPresent, setJob]);

  const update = useCallback(function (e) {
    const value = e.option || e.target.value;
    const key = e.target.getAttribute('name').replace(`${job._uid}-`, '');

    setJob(draft => { draft[key] = value });
  }, [setJob]);

  const remove = useCallback(function () {
    confirm(t('confrim')) && onRemove(index);
  }, [onRemove, index]);

  const updateLogo = useCallback(function (url) {
    setJob(draft => { draft.logoUrl = url });
    setShowLogoEditor(false);
  }, [setJob]);

  useEffect(function () {
    onChange(index, job);
  }, [job, index]);

  return (
    <Box className="job" direction="column" fill="horizontal">
      <LogoEditor
        show={showLogoEditor}
        onClose={toogleLogoEditor}
        onSubmit={updateLogo}
      />
      <Box className="comppany" direction="row">
        <Box
          className="company-logo"
          pad="small"
          alignSelf="center"
          onClick={toogleLogoEditor}
        >
          <AvatarUploadCover size={avatarSize}>
          {job.logoUrl ?
            <Avatar src={job.logoUrl} size={avatarSize} />
          : <Avatar background="status-unknown" size={avatarSize}>
              <Organization size={iconSize}/>
            </Avatar>
          }
          </AvatarUploadCover>
        </Box>
        <Box
          className="company-name"
          pad="small"
          flex="grow"
          direction="row"
          align="center"
        >
          <FullWidthFormField
            required={true}
            name={`${job._uid}-company`}
            validate={[
              function (value) {
                const len = value.trim().length;
                if (len > 25 && len < 1) {
                  return {
                    status: 'error',
                    message: t('limit-char-allowed', { number: 25 })
                  };
                }
              }
            ]}
          >
            <TextInput
              className="company-name-input"
              placeholder={t('company-name')}
              onChange={update}
              size={size}
              name={`${job._uid}-company`}
              value={job.company}
            />
          </FullWidthFormField>
        </Box>
      </Box>
      <Box className="job-title" direction="row" pad="small">
        <FullWidthFormField
          required={true}
          name={`${job._uid}-title`}
          validate={[
            function (value) {
              const len = value.trim().length;
              if (len > 25 && len < 1) {
                return {
                  status: 'error',
                  message: t('limit-char-allowed', { number: 25 })
                };
              }
            }
          ]}
        >
          <TextInput
            className="job-title-input"
            placeholder={t('job-title')}
            name={`${job._uid}-title`}
            onChange={update}
            value={job.title}
            size={size}
          />
        </FullWidthFormField>
      </Box>
      <Box className="date" direction="row" pad="small" gap="small">
        <FormField name={`${job._uid}-startMonth`} required={true} margin="none">
          <Select
            options={t('months')}
            size={size}
            onChange={update}
            name={`${job._uid}-startMonth`}
            value={job.startMonth}
            placeholder="Start Month"
          />
        </FormField>
        <FormField name={`${job._uid}-startYear`} required={true} margin="none">
          <Select
            options={YEARS}
            size={size}
            onChange={update}
            name={`${job._uid}-startYear`}
            value={job.startYear}
            placeholder={t('start-year')}
          />
        </FormField>
        <FormField name={`${job._uid}-endMonth`} required={!isPresent} margin="none">
          <Select
            options={t('months')}
            size={size}
            onChange={update}
            name={`${job._uid}-endMonth`}
            value={job.endMonth}
            disabled={isPresent}
            placeholder={t('end-month')}
          />
        </FormField>
        <FormField name={`${job._uid}-endYear`} required={!isPresent}margin="none">
          <Select
            options={YEARS}
            size={size}
            onChange={update}
            name={`${job._uid}-endYear`}
            value={job.endYear}
            disabled={isPresent}
            placeholder={t('end-year')}
          />
        </FormField>
        <CheckBox
          className="isPresent"
          checked={isPresent}
          label={t('present')}
          onChange={toggleIsPresent}
        />
      </Box>
      <Box
        className="job-description"
        direction="row"
        pad="small"
        gap="small"
      >
        <FullWidthFormField
          required={true}
          name={`${job._uid}-description`}
          validate={[
            function (value) {
              const len = value.trim().length;
              if (len > 300 && len < 1) {
                return {
                  status: 'error',
                  message: t('limit-char-allowed', { number: 300 })
                };
              }
            }
          ]}
        >
          <TextArea
            onChange={update}
            name={`${job._uid}-description`}
            value={job.description}
            placeholder={t('job-description')}
            size={size}
            resize="vertical"
          />
        </FullWidthFormField>
      </Box>
      <Footer pad="medium" justify="end" direction="row">
        <Button onClick={remove} color="status-critical" primary={true} label={t('remove')} />
      </Footer>
    </Box>
  );
}
