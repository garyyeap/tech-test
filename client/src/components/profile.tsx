import React, { useEffect, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useImmer } from 'use-immer';
import { Avatar, Button, Box, Footer, Form, FormField, TextInput } from 'grommet';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { User } from 'grommet-icons';
import useAdjustIconSize from '../hooks/use-adjust-icon-size';
import useAdjustSize from '../hooks/use-adjust-size';
import useResponsiveContext from '../hooks/use-responsive-context';
import { selectProfile } from '../reselect';
import AvatarUploadCover from './avatar-upload-cover';
import AvatarEditor from './avatar-editor';
import LoadingCover from './loading-cover';
import { fetchProfile, updateProfile } from '../apis/profile';

const MAX_YEAR = new Date().getFullYear() - 18;
const YEARBIRTH_REGEX = /(?<=\s|^)\d+(?=\s|$)/;

export default function () {
  const dispatch = useDispatch();
  const { id }: { id: string } = useParams();
  const { data, isPending } = useSelector(selectProfile);
  const [showAvatarEditor, setShowAvatarEditor] = useState(false);
  const [localData, setLocalData] = useImmer(data);
  const size = useResponsiveContext();
  const avatarSize = useAdjustSize(size, 1);
  const iconSize = useAdjustIconSize(avatarSize);
  const { t } = useTranslation('profile');

  const toogleAvatarEditor = useCallback(function () {
    setShowAvatarEditor(!showAvatarEditor);
  }, [showAvatarEditor]);

  const updateAvatar = useCallback(function (url) {
    setLocalData(draft => { draft.avatarUrl = url });
    setShowAvatarEditor(false);
  }, [setLocalData]);

  const updateName = useCallback(function (e) {
    const value = e.target.value;
    setLocalData(draft => { draft.name = value });
  }, [setLocalData]);

  const updateYearBirth = useCallback(function (e) {
    const value = e.target.value;
    setLocalData(draft => { draft.yearbirth = value });
  }, [setLocalData]);

  const submit = useCallback(function () {
    dispatch(updateProfile(id, localData));
  }, [localData]);

  useEffect(function () {
    dispatch(fetchProfile(id));
  }, [id]);

  useEffect(function () {
    if (!isPending) {
      setLocalData(() => data);
    }
  }, [isPending, data]);

  return (
    <LoadingCover show={isPending}>
      <Form onSubmit={submit}>
        <AvatarEditor
          show={showAvatarEditor}
          onClose={toogleAvatarEditor}
          onSubmit={updateAvatar}
        />
        <Box width="100%" direction="row" background="light-2" pad="medium">
          <Box
            id="avatar"
            pad="small"
            height="size"
            alignSelf="center"
            onClick={toogleAvatarEditor}
          >
            <AvatarUploadCover size={avatarSize}>
              {localData.avatarUrl ?
                <Avatar
                  background="status-unknown"
                  src={localData.avatarUrl}
                  size={avatarSize}
                />
              : <Avatar background="status-unknown" size={avatarSize}>
                  <User size={iconSize} />
                </Avatar>
              }
            </AvatarUploadCover>
          </Box>
          <Box id="name" pad="xsmall" flex="grow" direction="row" align="center">
            <Box id="name" pad="none" fill="horizontal" basis="3/4">
              <FormField
                required={true}
                name="name"
                validate={[
                  function (value) {
                    const len = value.trim().length;
                    if (len > 25 && len < 1) {
                      return {
                        status: 'error',
                        message: t('limit-char-allowed', { number: 25 })
                      };
                    }
                  }
                ]}
              >
                <TextInput
                  id="name-input"
                  placeholder={t('name')}
                  size={size}
                  name="name"
                  value={localData.name}
                  onChange={updateName}
                />
              </FormField>
            </Box>
            <Box id="yearbirth" pad="small" basis="1/4">
              <FormField
                required={true}
                name="yearbirth"
                validate={{ regexp: YEARBIRTH_REGEX, message: t('4-digi') }}
              >
                <TextInput
                  placeholder="1900"
                  size={size}
                  type="number"
                  name="yearbirth"
                  value={localData.yearbirth}
                  onChange={updateYearBirth}
                  min={1900}
                  max={MAX_YEAR}
                />
              </FormField>
            </Box>
          </Box>
        </Box>
        <Footer pad="medium" justify="end" direction="row" background="light-2">
          <Button primary={true} label={t('submit')} type="submit" />
        </Footer>
      </Form>
    </LoadingCover>
  );
}
