import React, { useCallback, useMemo } from 'react';
import { Header, Button, CheckBox, Box, Text, Stack } from 'grommet';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { patchAccount } from '../apis/account';
import { setContinuePath } from '../actions/misc';
import { selectAuth, selectMisc, selectAccount } from '../reselect';
// @ts-ignore: facebook sdk
import { fbLogout } from '../apis/facebook';

export default function () {
  const dispatch = useDispatch();
  const { pathname: path } = useLocation();
  const history = useHistory();
  const { uid } = useSelector(selectAuth);
  const { data, isPending } = useSelector(selectAccount);
  const { message } = useSelector(selectMisc);
  const { t } = useTranslation('auth');
  const [type, urlId] = useMemo(() => path.substring(1).split('/'), [path]);
  const isOwner = uid === urlId && !isPending;

  const update = useCallback(function () {
    dispatch(patchAccount(uid, { share: !data.share }));
  }, [data]);

  const login = useCallback(function () {
    dispatch(setContinuePath(path));
    history.push('/login');
  }, [path]);

  const edit = useCallback(function () {
    history.push(`/edit/${uid}`);
  }, [path]);

  const view = useCallback(function () {
    history.push(`/view/${uid}`);
  }, [path]);

  const logout = useCallback(function () {
    dispatch(fbLogout());
  }, []);

  return (
    <React.Fragment>
      <Stack anchor="top">
        <Header background="brand" fill="horizontal" pad="small" justify="end">
          {isOwner &&
            <CheckBox
              checked={data.share}
              onChange={update}
              toggle={true}
              label={t('public')}
              reverse={true}
            />
          }
          {!uid
            ? <Button secondary={true} label={t('login')} onClick={login}/>
            : (<React.Fragment>
                {type === 'view' && isOwner &&
                 <Button
                  primary={true}
                  label={t('edit')}
                  onClick={edit}
                  margin={{ horizontal: 'small' }}
                />}

                {type === 'edit' &&
                 <Button
                  primary={true}
                  label={t('view')}
                  onClick={view}
                  margin={{ horizontal: 'small' }}
                />}

                <Button secondary={true} label={t('logout')} onClick={logout}/>

              </React.Fragment>)
          }
        </Header>
      {message &&
        <Box
          background="status-ok"
          round={{ size: 'small', corner: 'bottom' }}
          pad="small"
          justify="center"
          animation="fadeIn"
        >
          <Text alignSelf="center">{message}</Text>
        </Box>}
      </Stack>
    </React.Fragment>
  );
}
