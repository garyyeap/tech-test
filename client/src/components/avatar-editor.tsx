import React, { useState, useCallback } from 'react';
import AvatarEditor from 'react-avatar-edit';
import { useTranslation } from 'react-i18next';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Layer
} from 'grommet';

interface AvatarEditorProps {
  show: boolean;
  onClose: () => void;
  onSubmit: (url: string) => void;
}
export default function ({ show, onClose, onSubmit }: AvatarEditorProps) {
  const { t } = useTranslation('avatar-editor');
  const [preview, setPreview] = useState(null);
  const update = useCallback(function (url) {
    setPreview(url);
  }, []);

  const submit = useCallback(function () {
    onSubmit(preview);
  }, [onSubmit, preview]);

  const checkFileSize = useCallback(function (e) {
    if (e.target.files[0].size > 71680) {
      alert(t('file-too-big'));
      e.target.value = '';
    }
  }, []);

  return (
    <React.Fragment>
      {show && <Layer onEsc={onClose} onClickOutside={onClose}>
        <Card id="avatar-editor" background="light-1" margin={{
          horizontal: 'auto',
          vertical: 'auto'
        }}>
          <CardBody pad="medium" flex="grow">
            <AvatarEditor
              width={256}
              height={256}
              onCrop={update}
              onBeforeFileLoad={checkFileSize}
            />
          </CardBody>
          <CardFooter pad="small" justify="end" background="light-4">
            <Button label={t('cancel')} onClick={onClose} />
            <Button primary={true} label={t('submit')} onClick={submit} />
          </CardFooter>
        </Card>
      </Layer>}
    </React.Fragment>
  );
}
