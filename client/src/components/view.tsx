import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Avatar, Box, Text, FormField, Header, Heading } from 'grommet';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { User } from 'grommet-icons';
import styled from 'styled-components';
import useAdjustIconSize from '../hooks/use-adjust-icon-size';
import useAdjustSize from '../hooks/use-adjust-size';
import useResponsiveContext from '../hooks/use-responsive-context';
import { selectJobs, selectProfile } from '../reselect';
import LoadingCover from './loading-cover';
import Job from './job-view';
import { fetchProfile } from '../apis/profile';
import { fetchJobs } from '../apis/jobs';

const FormFieldText = styled(FormField)`
  flex-grow: 1;
  & label { margin: 5px 0; font-weight: bold };
`;

export default function () {
  const dispatch = useDispatch();
  const { id }: { id: string } = useParams();
  const { data: profileData, isPending: profilePending } = useSelector(selectProfile);
  const { data: jobsData, isPending: jobsPending } = useSelector(selectJobs);
  const size = useResponsiveContext();
  const avatarSize = useAdjustSize(size, 1);
  const iconSize = useAdjustIconSize(avatarSize);
  const { t: profileT } = useTranslation('profile');
  const { t: jobsT } = useTranslation('jobs');

  const jobs = useMemo(function () {
    return jobsData.map((job, index) => <Job job={job} key={index} />);
  }, [jobsData]);

  useEffect(function () {
    dispatch(fetchProfile(id));
    dispatch(fetchJobs(id));
  }, [id]);

  return (
    <React.Fragment>
      <LoadingCover show={profilePending}>
        <Box width="100%" direction="row" background="light-2" pad="medium">
          <Box
            id="avatar"
            pad="small"
            height="size"
            alignSelf="center"
          >
            {profileData.avatarUrl ?
              <Avatar
                background="status-unknown"
                src={profileData.avatarUrl}
                size={avatarSize}
              />
            : <Avatar background="status-unknown" size={avatarSize}>
                <User size={iconSize} />
              </Avatar>
            }
          </Box>
          <Box id="name" pad="xsmall" flex="grow" direction="row" align="center">
            <Box id="name" pad="none" fill="horizontal" basis="3/4">
              <FormFieldText label={profileT('name')}>
                <Text margin="10px" size={size}>{profileData.name}</Text>
              </FormFieldText>
            </Box>
            <Box id="yearbirth" pad="small" basis="1/4">
              <FormFieldText label={profileT('yearbirth')}>
                <Text margin="10px" size={size}>{profileData.yearbirth}</Text>
              </FormFieldText>
            </Box>
          </Box>
        </Box>
      </LoadingCover>
      <LoadingCover show={jobsPending}>
        <Header pad="medium" justify="between" direction="row">
          <Heading truncate={true} level="2">{jobsT('experiences')}</Heading>
        </Header>
        <Box width="100%" direction="column">{jobs}</Box>
      </LoadingCover>
    </React.Fragment>
  );
}
