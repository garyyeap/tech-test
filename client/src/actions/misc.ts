export const SET_CONTINUE_PATH = 'SET_CONTINUE_PATH';
export const NOTIFY = 'NOTIFY';
export const DISMISS = 'DISMISS';
export const RESTORE_SUCCESS = 'RESTORE_SUCCESS';
export const RESTORE_ERROR = 'RESTORE_ERROR';
export const RESTORE_PENDING = 'RESTORE_PENDING';

export function setContinuePath (continuePath?: string) {
  return {
    type: SET_CONTINUE_PATH,
    continuePath
  };
}

export function notify (message: string) {
  return {
    type: NOTIFY,
    message
  };
}

export function dismiss () {
  return {
    type: DISMISS
  };
}

export function restoreSuccess () {
  return {
    type: RESTORE_SUCCESS
  };
}

export function restoreError () {
  return {
    type: RESTORE_ERROR
  };
}

export function restorePending () {
  return {
    type: RESTORE_PENDING
  };
}
