export const FETCH_PROFILE_PENDING = 'FETCH_PROFILE_PENDING';
export const FETCH_PROFILE_SUCCESS = 'FETCH_PROFILE_SUCCESS';
export const FETCH_PROFILE_ERROR = 'FETCH_PROFILE_ERROR';
export const UPDATE_PROFILE_ERROR = 'UPDATE_PROFILE_ERROR';
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_PENDING = 'UPDATE_PROFILE_PENDING';

export function fetchPending () {
  return {
    type: FETCH_PROFILE_PENDING
  };
}

export function fetchSuccess (data: object) {
  return {
    type: FETCH_PROFILE_SUCCESS,
    data
  };
}

export function fetchError (reason: string) {
  return {
    type: FETCH_PROFILE_ERROR,
    reason
  };
}

export function updatePending () {
  return {
    type: UPDATE_PROFILE_PENDING
  };
}

export function updateSuccess (data: object) {
  return {
    type: FETCH_PROFILE_SUCCESS,
    data
  };
}

export function updateError (reason: string) {
  return {
    type: UPDATE_PROFILE_ERROR,
    reason
  };
}
