export const FB_LOGIN_PENDING = 'FB_LOGIN_PENDING';
export const FB_LOGIN_SUCCESS = 'FB_LOGIN_SUCCESS';
export const FB_LOGIN_ERROR = 'FB_LOGIN_ERROR';
export const FB_LOGOUT_PENDING = 'FB_LOGOUT_PENDING';
export const FB_LOGOUT_SUCCESS = 'FB_LOGOUT_SUCCESS';

export function fbLoginPending () {
  return {
    type: FB_LOGIN_PENDING
  };
}

export function fbLoginSuccess (data: object) {
  return {
    type: FB_LOGIN_SUCCESS,
    ...data
  };
}

export function fbLoginError (reason: string) {
  return {
    type: FB_LOGIN_ERROR,
    reason
  };
}

export function fbLogoutPending () {
  return {
    type: FB_LOGOUT_PENDING
  };
}

export function fbLogoutSuccess () {
  return {
    type: FB_LOGOUT_SUCCESS
  };
}
