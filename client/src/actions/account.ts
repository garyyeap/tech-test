export const FETCH_ACCOUNT_PENDING = 'FETCH_ACCOUNT_PENDING';
export const FETCH_ACCOUNT_SUCCESS = 'FETCH_ACCOUNT_SUCCESS';
export const FETCH_ACCOUNT_ERROR = 'FETCH_ACCOUNT_ERROR';
export const PATCH_ACCOUNT_PENDING = 'PATCH_ACCOUNT_PENDING';
export const PATCH_ACCOUNT_SUCCESS = 'PATCH_ACCOUNT_SUCCESS';
export const PATCH_ACCOUNT_ERROR = 'PATCH_ACCOUNT_ERROR';

export function fetchPending () {
  return {
    type: FETCH_ACCOUNT_PENDING
  };
}

export function fetchSuccess (data: Acc) {
  return {
    type: FETCH_ACCOUNT_SUCCESS,
    data
  };
}

export function fetchError (reason: string) {
  return {
    type: FETCH_ACCOUNT_ERROR,
    reason
  };
}

export function patchPending () {
  return {
    type: PATCH_ACCOUNT_PENDING
  };
}

export function patchSuccess (data: object) {
  return {
    type: PATCH_ACCOUNT_SUCCESS,
    data
  };
}

export function patchError (reason: string) {
  return {
    type: PATCH_ACCOUNT_ERROR,
    reason
  };
}
