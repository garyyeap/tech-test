export const FETCH_JOBS_PENDING = 'FETCH_JOBS_PENDING';
export const FETCH_JOBS_SUCCESS = 'FETCH_JOBS_SUCCESS';
export const FETCH_JOBS_ERROR = 'FETCH_JOBS_ERROR';
export const UPDATE_JOBS_ERROR = 'UPDATE_JOBS_ERROR';
export const UPDATE_JOBS_SUCCESS = 'UPDATE_JOBS_SUCCESS';
export const UPDATE_JOBS_PENDING = 'UPDATE_JOBS_PENDING';

export function fetchPending () {
  return {
    type: FETCH_JOBS_PENDING
  };
}

export function fetchSuccess (data: Job[]) {
  return {
    type: FETCH_JOBS_SUCCESS,
    data
  };
}

export function fetchError (reason: string) {
  return {
    type: FETCH_JOBS_ERROR,
    reason
  };
}

export function updatePending () {
  return {
    type: UPDATE_JOBS_PENDING
  };
}

export function updateSuccess (data: object) {
  return {
    type: UPDATE_JOBS_SUCCESS,
    data
  };
}

export function updateError (reason: string) {
  return {
    type: UPDATE_JOBS_ERROR,
    reason
  };
}
