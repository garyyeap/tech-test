export const HAS_ERROR = 'HAS_ERROR';
export const DISMISS_ERROR = 'DISMISS_ERROR';

export function hasError (reason: string, redirect?: string) {
  return {
    type: HAS_ERROR,
    reason,
    redirect
  };
}

export function dismissError () {
  return {
    type: DISMISS_ERROR
  };
}
