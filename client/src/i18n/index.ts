import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import en from './en.json';

i18n.use(initReactI18next).use(LanguageDetector).init({
  resources: {
    en
  },
  fallbackLng: 'en',
  keySeparator: false,
  returnObjects: true,
  interpolation: {
    escapeValue: false
  }
}, (err) => err && console.log(err));

export const getTranslate = function (namespace: string, status: number | string): string {
  const key = `${namespace}:${status}`;
  return i18n.exists(key) ? i18n.t(key) : i18n.t(`error:fallback`);
}

export default i18n;
