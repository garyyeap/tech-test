import fetch from './fetch';
import { getTranslate } from '../i18n';
import { hasError } from '../actions/error';
import { updateSuccess as profileSuccess } from '../actions/profile';
import { updateSuccess as jobsSuccess } from '../actions/jobs';
import { patchSuccess as accountSuccess } from '../actions/account';
import {
  restoreSuccess,
  restorePending,
  restoreError,
  notify,
  dismiss
} from '../actions/misc';

export function cancelBackup () {
  return function () {
    window.localStorage.removeItem('has-backup');
    window.localStorage.removeItem('profile-backup');
    window.localStorage.removeItem('jobs-backup');
    window.localStorage.removeItem('account-backup');
  };
}

export function restoreBackup () {
  return async function (dispatch: Dispatch) {
    dispatch(restorePending());

    const id = window.localStorage.getItem('has-backup');
    const profile = window.localStorage.getItem('profile-backup');
    const jobs = window.localStorage.getItem('jobs-backup');
    const account = window.localStorage.getItem('account-backup');
    const fetchTasks = [];

    if (profile) {
      fetchTasks.push(fetch(`users/${id}/profile`, {
        method: 'PUT',
        body: profile
      }).then(function (response) {
        if (response.ok) {
          dispatch(profileSuccess(JSON.parse(profile)));
          window.localStorage.removeItem('profile-backup');
        }

        return response;
      }));
    }

    if (jobs) {
      fetchTasks.push(fetch(`users/${id}/jobs`, {
        method: 'PUT',
        body: jobs
      }).then(function (response) {
        if (response.ok) {
          dispatch(jobsSuccess(JSON.parse(jobs)));
          window.localStorage.removeItem('jobs-backup');
        }

        return response;
      }));
    }

    if (account) {
      fetchTasks.push(fetch(`users/${id}`, {
        method: 'PATCH',
        body: account
      }).then(function (response) {
        if (response.ok) {
          dispatch(accountSuccess(JSON.parse(account)));
          window.localStorage.removeItem('account-backup');
        }

        return response;
      }));
    }

    const responses = await Promise.all(fetchTasks);

    if (responses.every(response => response.ok)) {
      window.localStorage.removeItem('has-backup');
      dispatch(restoreSuccess());
      dispatch(notify(getTranslate('misc', 'backup-done')));
      setTimeout(() => dispatch(dismiss()), 2000);
    } else {
      dispatch(hasError(getTranslate('misc', 'backup-failed')));
      dispatch(restoreError());
    }
  };
}
