import { API_HOST } from '../constants';

export const headers = new Headers({ 'Content-Type': 'application/json' });

if (window.localStorage.getItem('token')) {
  headers.append('Token', window.localStorage.getItem('token'));
}

export default function (url: string, opt?: RequestInit): Promise<Response>{
  return new Promise (function (resolve, reject) {
    const request = new Request(`${API_HOST}/${url}`, Object.assign({
      headers
    }, opt));
    
    window.setTimeout(reject, 3000);
    fetch(request).then(resolve, reject);
  });
}
