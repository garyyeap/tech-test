import fetch from './fetch';
import { hasError } from '../actions/error';
import { notify, dismiss } from '../actions/misc';
import {
  fetchPending,
  fetchSuccess,
  fetchError,
  patchPending,
  patchSuccess,
  patchError
} from '../actions/account';
import { getTranslate } from '../i18n';

export function fetchAccount (id: string) {
  return async function (dispatch: Dispatch, getState: Function) {
    dispatch(fetchPending());

    try {
      const response = await fetch(`users/${id}`, {
        method: 'GET'
      });

      if (response.ok) {
        const body = await response.json();
        dispatch(fetchSuccess(body));
      } else {
        const status = response.status;
        const reason = getTranslate('error', status);
        const redirect = (status === 404 || status === 401) ? '/login' : null;
        dispatch(hasError(reason, redirect));
        dispatch(fetchError(reason));
      }

      return response;
    } catch (e) {
      dispatch(fetchSuccess(getState().account.data));
    }
  };
}

export function patchAccount (id: string, body: Acc) {
  return async function (dispatch: Dispatch) {
    dispatch(patchPending());

    try {
      const response = await fetch(`users/${id}`, {
        method: 'PATCH',
        body: JSON.stringify(body)
      });

      if (response.ok) {
        body.uid = id;
        dispatch(patchSuccess(body));
        window.localStorage.removeItem('account-backup');
        dispatch(notify(getTranslate('misc', 'updated')));
        setTimeout(() => dispatch(dismiss()), 1500);
      } else {
        const reason = getTranslate('error', response.status);

        dispatch(hasError(reason));
        dispatch(patchError(reason));
      }

      return response;
    } catch (e) {
      window.localStorage.setItem('has-backup', id);
      window.localStorage.setItem('account-backup', JSON.stringify(body));
      dispatch(patchSuccess(body));
      dispatch(notify(getTranslate('misc', 'backup-done')));
      setTimeout(() => dispatch(dismiss()), 1500);
    }
  };
}
