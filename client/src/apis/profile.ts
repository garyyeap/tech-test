import fetch from './fetch';
import { hasError } from '../actions/error';
import { notify, dismiss } from '../actions/misc';
import { getTranslate } from '../i18n';
import {
  fetchPending,
  fetchSuccess,
  fetchError,
  updatePending,
  updateSuccess,
  updateError
} from '../actions/profile';

export function fetchProfile (id: string) {
  return async function (dispatch: Dispatch, getState: Function) {
    dispatch(fetchPending());

    try {
      const response = await fetch(`users/${id}/profile`);

      if (response.ok) {
        const body = await response.json();

        dispatch(fetchSuccess(body));
      } else {
        const { status } = response;
        const reason = getTranslate('error', status);
        const redirect = (status === 404 || status === 401) ? '/login' : null;

        dispatch(hasError(reason, redirect));
        dispatch(fetchError(reason));
      }
    } catch (e) {
      dispatch(fetchSuccess(getState().profile.data));
    }
  };
}

export function updateProfile (id: string, body: Profile) {
  return async function (dispatch: Dispatch) {
    dispatch(updatePending());

    try {
      const response = await fetch(`users/${id}/profile`, {
        method: 'PUT',
        body: JSON.stringify(body)
      });

      if (response.ok) {
        dispatch(updateSuccess(body));
        window.localStorage.removeItem('profile-backup');
        dispatch(notify(getTranslate('misc', 'updated')));
        setTimeout(() => dispatch(dismiss()), 2000);
      } else {
        const { status } = response;
        const reason = getTranslate('error', status);
        const redirect = (status === 404 || status === 401) ? '/login' : null;

        dispatch(hasError(reason, redirect));
        dispatch(updateError(reason));
      }
    } catch {
      window.localStorage.setItem('has-backup', id);
      window.localStorage.setItem('profile-backup', JSON.stringify(body));
      dispatch(updateSuccess(body));
      dispatch(notify(getTranslate('misc', 'backup-done')));
      setTimeout(() => dispatch(dismiss()), 1500);
      return;
    }
  };
}
