import {
  fbLoginPending,
  fbLoginSuccess,
  fbLoginError,
  fbLogoutPending,
  fbLogoutSuccess
} from '../actions/auth';
import fetch, { headers } from './fetch';
import { FB_APP_ID } from '../constants';

(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;

  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

const loadSdk = new Promise(function (resolve) {
  window.fbAsyncInit = function () {

    window.FB.init({
      appId: FB_APP_ID,
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v8.0'
    });

    resolve(window.FB);
  };
});

const login = async function () {
  const FB = await loadSdk;
  return new Promise(function (resolve) {
    FB.login((response) => resolve(response));
  });
};

const fetchEmail = async function (token) {
  const FB = await loadSdk;
  const params = {
      fields: 'id,email,name',
      accessToken: token
  };

  return new Promise(function (resolve) {
    FB.api('/me', 'GET', params, (response) => resolve(response));
  }, { scope: 'email' });
};

const logout = async function () {
  const FB = await loadSdk;

  FB.logout(function () {});
}

const sleep = (time) => new Promise(resolve => setTimeout(resolve, time));

export function fbLogin () {
  return async function (dispatch) {
    dispatch(fbLoginPending());

    const response = await login();

    if (response.status !== 'connected') {
      dispatch(fbLoginError(response));

      return Promise.resolve();
    }

    const { authResponse } = response;
    const data = {
      token: authResponse.accessToken,
      uid: authResponse.userID,
      tokenExpires: (Date.now() + parseInt(authResponse.expiresIn) * 1000)
    };

    headers.append('Token', data.token);

    const userRes = await fetch(`users/${data.uid}`);

    if (!userRes.ok) {
      const result = await fetch('users', {
        method: 'POST',
        body: JSON.stringify({
          id: data.uid,
          vendor: 'fb',
          token: data.token,
          share: false
        })
      });
      data.share = false;

      if (!result.ok) {
        return dispatch(fbLoginError());
      }
    }

    dispatch(fbLoginSuccess(data));
  }
}

export function fbLogout () {
  return async function (dispatch) {
    dispatch(fbLogoutPending());

    await logout();

    dispatch(fbLogoutSuccess());
    window.location.href = '/';
  }
}
