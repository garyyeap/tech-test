import fetch from './fetch';
import { hasError } from '../actions/error';
import { notify, dismiss } from '../actions/misc';
import {
  fetchPending,
  fetchSuccess,
  fetchError,
  updatePending,
  updateSuccess,
  updateError
} from '../actions/jobs';
import { getTranslate } from '../i18n';

export function fetchJobs (id: string) {
  return async function (dispatch: Dispatch, getState: Function) {
    dispatch(fetchPending());

    try {
      const response = await fetch(`users/${id}/jobs`);

      if (response.ok) {
        const body = await response.json();
        dispatch(fetchSuccess(body));
      } else {
        const reason = getTranslate('error', response.status);

        dispatch(hasError(reason));
        dispatch(fetchError(reason));
      }
    } catch (e) {
      dispatch(fetchSuccess(getState().jobs.data));
    }
  };
}

export function updateJobs (id: string, body: Job[]) {
  return async function (dispatch: Dispatch) {
    dispatch(updatePending());

    try {
      const response = await fetch(`users/${id}/jobs`, {
        method: 'PUT',
        body: JSON.stringify(body)
      });

      if (response.ok) {
        window.localStorage.removeItem('jobs-backup');
        dispatch(updateSuccess(body));
        dispatch(notify(getTranslate('misc', 'updated')));
        setTimeout(() => dispatch(dismiss()), 1500);
      } else {
        const { status } = response;
        const reason = getTranslate('error', status);
        const redirect = (status === 404 || status === 401) ? '/login' : null;

        dispatch(hasError(reason, redirect));
        dispatch(updateError(reason));
      }
    } catch (e) {
      window.localStorage.setItem('has-backup', id);
      window.localStorage.setItem('jobs-backup', JSON.stringify(body));

      dispatch(updateSuccess(body));
      dispatch(notify(getTranslate('misc', 'backup-done')));
      setTimeout(() => dispatch(dismiss()), 1500);
    }
  };
}
