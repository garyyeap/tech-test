import Redux from 'redux';
import { _SIZE } from './constants';

declare global {
  type Size = typeof _SIZE[number];
  type Dispatch = Redux.Dispatch;

  interface Job {
    [key: string]: string | boolean | number;
    _uid?: number;
    title?: string;
    startYear?: string;
    endYear?: string;
    startMonth?: string;
    endMonth?: string;
    present: boolean;
    description?: string;
    logoUrl?: string;
    company?: string;
  }

  interface Profile {
    name?: string;
    yearbirth?: number;
    avatarUrl?: string;
  }

  interface Acc {
    share?: boolean;
    uid?: string;
  }

  interface AccState {
    data: Acc;
    isPending: boolean;
    hasError: boolean;
    reason?: string;
  }

  interface ProfileState {
    data: Profile;
    isPending: boolean;
    hasError: boolean;
    reason?: string;
  }

  interface JobsState {
    data: Job[];
    isPending: boolean;
    hasError: boolean;
    reason?: string;
  }

  interface AuthState {
    vendor?: string;
    uid?: string;
    token?: string;
    isPending: boolean;
    hasError: boolean;
    reason?: string;
    tokenExpires?: string;
  }

  interface MiscState {
    continuePath?: string;
    message?: string;
    isRestorePending?: boolean;
  }

  interface ErrorState {
    hasError?: boolean;
    reason?: string;
    redirect?: string;
    continuePath?: string;
  }

  interface State {
    account: AccState;
    profile: ProfileState;
    jobs: JobsState;
    error: ErrorState;
    auth: AuthState;
    misc: MiscState;
  }
}
