import React from 'react';
import Thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { Grommet } from 'grommet';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import './i18n';
import theme from './theme';
import Layout from './layout';
import Error from './components/error';
import rootReducer from './reducers';

const store = createStore(rootReducer, applyMiddleware(Thunk));

export default function () {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <Grommet theme={theme} plain={false} full={true}>
          <Layout />
          <Error />
        </Grommet>
      </Provider>
    </BrowserRouter>
  );
}
