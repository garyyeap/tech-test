import ReactDOM from "react-dom";
import React from "react";
import Main from './src/main';

ReactDOM.render(
  <Main />,
  document.getElementById('main')
);
