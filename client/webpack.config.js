require('dotenv').config();
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { argv } = require('yargs');

const configs = {
  entry: [
    path.join(__dirname, 'index.tsx')
  ],
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './dist'),
    publicPath: '/'
  },
  resolve: {
    extensions: ['.ts',  '.tsx', '.js', '.json', 'd.ts'],
    modules: ['node_modules']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /(node_modules)/,
        use: ['babel-loader','ts-loader']
      },
      {
        test: /\.js?$/,
        exclude: /(node_modules)/,
        use: ['babel-loader']
      }
    ]
  },
  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'template.html')
    }),
    new webpack.DefinePlugin({
      GLOBAL_API_HOST: JSON.stringify(process.env.API_HOST),
      GLOBAL_FB_APP_ID: JSON.stringify(process.env.FB_APP_ID)
    })
  ],
  devtool: false
};

if (argv.dev) {
  configs.watch = true;
  configs.entry.push('webpack/hot/dev-server');
  configs.devServer = {
    contentBase: './dist',
    historyApiFallback: true
  };
  configs.devtool = 'source-map';
}

if (argv.mock) {
  configs.entry.push(path.join(__dirname, 'mock-server.js'));
}

module.exports = configs;
