import { match } from 'path-to-regexp';

const canAccess = function ({ id, share }, requestId) {
  const isOwnProfile = id === requestId;
  return isOwnProfile || share;
}

const responseOk = function (data) {
  return Object.assign({
    status: 200,
    headers: {'Content-Type': 'application/json'}
  }, data);
};

export default function (fetchMock, data) {
  fetchMock.get('express:/users/:id', function (url, opt, request) {
    const id = match('/users/:id')(new URL(url).pathname).params.id;
    const user = data[id];

    if (!user) {
      return { status: 404 };
    }

    if (!canAccess(user.account, id)) {
      return { status: 401 };
    }

    return responseOk({
      body: {
        uid: id,
        share: user.account.share,
      }
    });
  });

  fetchMock.get('express:/users/:id/profile', function (url, opt, request) {
    const id = match('/users/:id/profile')(new URL(url).pathname).params.id;
    const user = data[id];

    if (!user) {
      return { status: 404 };
    }

    if (!canAccess(user.account, id)) {
      return { status: 401 };
    }

    return responseOk({
      body: user.profile,
    });
  });

  fetchMock.get('express:/users/:id/jobs', function (url, opt, request) {
    const id = match('/users/:id/jobs')(new URL(url).pathname).params.id;
    const user = data[id];

    if (!user) {
      return { status: 404 };
    }

    if (!canAccess(user.account, id)) {
      return { status: 401 };
    }

    return responseOk({
      body: user.jobs,
    });
  });

  fetchMock.post('express:/users', async function (url, opt, request) {
    const { id, email, token, vendor } = await request.json();

    if (!email || !token || !vendor) {
      return { status: 400 };
    }

    data[id] = {
      account: {
        id,
        email,
        token,
        vendor,
        share: false
      }
    };

    return { status: 204 };
  });

  fetchMock.patch('express:/users/:id', async function (url, opt, request) {
    const id = match('/users/:id')(new URL(url).pathname).params.id;
    const user = data[id];

    if (!user) {
      return { status: 404 };
    }

    if (!canAccess(user.account, id)) {
      return { status: 401 };
    }

    const { share, token, email } = await request.json();

    if (typeof share === 'boolean') {
      user.account.share = share;
    }

    if (token) {
      user.account.token = token;
    }

    if (email) {
      user.account.email = email;
    }

    return { status: 204 };
  });

  fetchMock.put('express:/users/:id/profile', async function (url, opt, request) {
    const id = match('/users/:id/profile')(new URL(url).pathname).params.id;
    const user = data[id];

    if (!user) {
      return { status: 404 };
    }

    if (!canAccess(user.account, id)) {
      return { status: 401 };
    }

    const { name, yearbirth, avatarUrl } = await request.json();

    user.profile = { name, yearbirth, avatarUrl };

    return { status: 204 };
  });

  fetchMock.put('express:/users/:id/jobs', async function (url, opt, request) {
    const id = match('/users/:id/jobs')(new URL(url).pathname).params.id;
    const user = data[id];

    if (!user) {
      return { status: 404 };
    }

    if (!canAccess(user.account, id)) {
      return { status: 401 };
    }

    user.jobs = await request.json();

    return { status: 204 };
  });
}
