import fetchMock from 'fetch-mock';
import server from './tests/apis/server';

server(fetchMock, {
  id: {
    account: {
      id: null,
      vendor: null,
      token: null,
      share: false,
      email: null
    },
    profile: {
      name: null,
      yearbirth: null,
      avatarUrl: null
    },
    jobs: []
  }
});
