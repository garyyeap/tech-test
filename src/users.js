module.exports = function (app, db) {
  app.get('/users/:id', function (req, res) {
    const id = req.params.id;

    return res.status(200).json({
      uid: id,
      share: req.user.account.share
    });
  });

  app.post('/users', async function (req, res) {
    if (!req.validId) {
      return res.status(401).end();
    }

    db.set(`users[${req.validId}]`, {
      account: {
        email: req.email,
        id: req.validId,
        share: false,
        token: req.header('token'),
        vendor: "fb"
      },
      profile: {
        name: req.name,
        yearbirth: 1900,
        avatarUrl: null
      },
      jobs: []
    }).write();

    return res.status(204).end();
  });

  app.patch('/users/:id', async function (req, res, next) {
    const { share, token, email } = req.body;
    const body = {};
    if (typeof share === 'boolean') {
      req.user.account.share = share;
      body.share = share;
    }

    if (token) {
      req.user.account.token = token;
      body.token = token;
    }

    if (email) {
      req.user.account.email = email;
      body.email = email;
    }

    db.set(`users[${req.validId}]`, req.user).write();
    body.uid = req.validId;

    return res.status(204).json(body);
  });
};
