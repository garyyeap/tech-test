module.exports = function (app, db) {
  app.get('/users/:id/jobs', function (req, res) {

    return res.status(200).json(req.user.jobs);
  });

  app.put('/users/:id/jobs', async function (req, res) {
    req.user.jobs = req.body;

    db.set(`users[${req.validId}]`, req.user).write();

    return res.status(200).json(req.user.jobs);
  });
};
