
module.exports = function (app, db) {
  app.put('/users/:id/profile', async function (req, res) {
    const { name, yearbirth, avatarUrl } = req.body;
    const id = req.validId;

    req.user.profile = { name, yearbirth, avatarUrl };

    db.set(`users[${id}]`, req.user).write();

    return res.status(204).end();
  });

  app.get('/users/:id/profile', function (req, res) {
    return res.status(200).json(req.user.profile);
  });
};
