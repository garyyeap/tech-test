# Start api server

```
cd api
npm install
node index.js
```

# Start client server
1. Add a .env file to `./client/`: `./client/.env`
2. The content should be:
```
API_HOST=localhost:5566
FB_APP_ID=404650210518389
```
3. Then
```
cd client
npm install
npm start
```

4. Or start a client based mock server
```
npm run start-mock
```
5. The url is http://localhost:8080
